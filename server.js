require("dotenv").config();
var jwt = require("jsonwebtoken");
var uuid4 = require("uuid4");
var cors = require("cors");

const express = require("express");

const app = express();

var app_access_key = process.env.APP_ACCESS_KEY;
var app_secret = process.env.APP_SECRET;
app.use(cors());
var payload = {
  access_key: app_access_key,
  type: "management",
  version: 2,
  iat: Math.floor(Date.now() / 1000),
  nbf: Math.floor(Date.now() / 1000),
};

app.get("/", function (req, res) {
  const token = jwt.sign(payload, app_secret, {
    algorithm: "HS256",
    expiresIn: "24h",
    jwtid: uuid4(),
  });

  res.status(200).send({ token: token });
});

app.listen(5000);
